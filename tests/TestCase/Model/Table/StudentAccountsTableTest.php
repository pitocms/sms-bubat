<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StudentAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StudentAccountsTable Test Case
 */
class StudentAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StudentAccountsTable
     */
    public $StudentAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.StudentAccounts',
        'app.Months',
        'app.Students'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('StudentAccounts') ? [] : ['className' => StudentAccountsTable::class];
        $this->StudentAccounts = TableRegistry::getTableLocator()->get('StudentAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StudentAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
