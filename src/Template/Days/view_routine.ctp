
<?php 
    //debug($days);
    // foreach ($days as $key => $day) {
    //     echo $day->name;
    //     foreach ($day->routines as $key => $value) {
    //         echo $value->room->room_number;
    //     }
    // }
?>
<div class="rooms form large-9 medium-8 columns content">
<table class="table table-bordered">
  <?php foreach ($days as $key => $day) { ?>
  <tr>
    <th><?= $day->name ?></th>
    <?php foreach ($day->routines as $key => $value) { ?>
    <td class="text-center">
    	 <?php //debug($value); ?>
    	 <?= $value->subject->name." ".$value->subject->subject_code ?>	
    	 <?= "<br>".$value->starting_hour->format('h:ia')." - ".$value->ending_hour->format('h:ia');; ?>
    	 <?= "<br>".$value->teacher->name ?>	
    	 <?= "<br>Room:".$value->room->room_number ?>	 
    </td>
    <?php } ?>
  </tr>
  <?php } ?>
</table>
</div>