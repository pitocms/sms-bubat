<div class="days view large-9 medium-8 columns content">
    <h3><?= h($day->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($day->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($day->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($day->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($day->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Routines') ?></h4>
        <?php if (!empty($day->routines)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Department Id') ?></th>
                <th scope="col"><?= __('Semester Id') ?></th>
                <th scope="col"><?= __('Subject Id') ?></th>
                <th scope="col"><?= __('Teacher Id') ?></th>
                <th scope="col"><?= __('Room Id') ?></th>
                <th scope="col"><?= __('Day Id') ?></th>
                <th scope="col"><?= __('Starting Hour') ?></th>
                <th scope="col"><?= __('Ending Hour') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($day->routines as $routines): ?>
            <tr>
                <td><?= h($routines->id) ?></td>
                <td><?= h($routines->department_id) ?></td>
                <td><?= h($routines->semester_id) ?></td>
                <td><?= h($routines->subject_id) ?></td>
                <td><?= h($routines->teacher_id) ?></td>
                <td><?= h($routines->room_id) ?></td>
                <td><?= h($routines->day_id) ?></td>
                <td><?= h($routines->starting_hour) ?></td>
                <td><?= h($routines->ending_hour) ?></td>
                <td><?= h($routines->created) ?></td>
                <td><?= h($routines->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Routines', 'action' => 'view', $routines->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Routines', 'action' => 'edit', $routines->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Routines', 'action' => 'delete', $routines->id], ['confirm' => __('Are you sure you want to delete # {0}?', $routines->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
