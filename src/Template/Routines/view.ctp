<div class="routines view large-9 medium-8 columns content">
    <h3><?= h($routine->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Department') ?></th>
            <td><?= $routine->has('department') ? $this->Html->link($routine->department->name, ['controller' => 'Departments', 'action' => 'view', $routine->department->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Semester') ?></th>
            <td><?= $routine->has('semester') ? $this->Html->link($routine->semester->name, ['controller' => 'Semesters', 'action' => 'view', $routine->semester->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subject') ?></th>
            <td><?= $routine->has('subject') ? $this->Html->link($routine->subject->name, ['controller' => 'Subjects', 'action' => 'view', $routine->subject->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Teacher') ?></th>
            <td><?= $routine->has('teacher') ? $this->Html->link($routine->teacher->name, ['controller' => 'Teachers', 'action' => 'view', $routine->teacher->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Room') ?></th>
            <td><?= $routine->has('room') ? $this->Html->link($routine->room->id, ['controller' => 'Rooms', 'action' => 'view', $routine->room->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Day') ?></th>
            <td><?= $routine->has('day') ? $this->Html->link($routine->day->name, ['controller' => 'Days', 'action' => 'view', $routine->day->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($routine->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Starting Hour') ?></th>
            <td><?= h($routine->starting_hour) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ending Hour') ?></th>
            <td><?= h($routine->ending_hour) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($routine->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($routine->modified) ?></td>
        </tr>
    </table>
</div>
