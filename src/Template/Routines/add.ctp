<div class="routines form large-9 medium-8 columns content">
    <?= $this->Form->create($routine) ?>
    <fieldset>
        <legend><?= __('Add Routine') ?></legend>
        <?php
            echo $this->Form->control('department_id', ['options' => $departments]);
            echo $this->Form->control('semester_id', ['options' => $semesters]);
            echo $this->Form->control('subject_id', ['options' => $subjects]);
            echo $this->Form->control('teacher_id', ['options' => $teachers]);
            echo $this->Form->control('room_id', ['options' => $rooms]);
            echo $this->Form->control('day_id', ['options' => $days]);
            echo $this->Form->control('starting_hour');
            echo $this->Form->control('ending_hour');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
