<div class="routines index large-9 medium-8 columns content">
    <h3><?= __('Routines') ?></h3>

    <table class="table table-striped table-bordered table-centered">
            <?php foreach ($days as $key => $day) { ?>
                <tr>
                    <td><?= $day->name ?></td>
                    <td><span style="border:1px solid red">Rot-1</span> <span style="border:1px solid red">Rot -2</span></td>
                </tr>
            <?php } ?>
    </table>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('department_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('semester_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subject_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('teacher_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('room_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('day_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('starting_hour') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ending_hour') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($routines as $routine): ?>
            <tr>
                <td><?= $this->Number->format($routine->id) ?></td>
                <td><?= $routine->has('department') ? $this->Html->link($routine->department->name, ['controller' => 'Departments', 'action' => 'view', $routine->department->id]) : '' ?></td>
                <td><?= $routine->has('semester') ? $this->Html->link($routine->semester->name, ['controller' => 'Semesters', 'action' => 'view', $routine->semester->id]) : '' ?></td>
                <td><?= $routine->has('subject') ? $this->Html->link($routine->subject->name, ['controller' => 'Subjects', 'action' => 'view', $routine->subject->id]) : '' ?></td>
                <td><?= $routine->has('teacher') ? $this->Html->link($routine->teacher->name, ['controller' => 'Teachers', 'action' => 'view', $routine->teacher->id]) : '' ?></td>
                <td><?= $routine->has('room') ? $this->Html->link($routine->room->id, ['controller' => 'Rooms', 'action' => 'view', $routine->room->id]) : '' ?></td>
                <td><?= $routine->has('day') ? $this->Html->link($routine->day->name, ['controller' => 'Days', 'action' => 'view', $routine->day->id]) : '' ?></td>
                <td><?= h($routine->starting_hour) ?></td>
                <td><?= h($routine->ending_hour) ?></td>
                <td><?= h($routine->created) ?></td>
                <td><?= h($routine->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $routine->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $routine->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $routine->id], ['confirm' => __('Are you sure you want to delete # {0}?', $routine->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
