<div class="studentAccounts form large-9 medium-8 columns content">
    <?= $this->Form->create($studentAccount) ?>
    <fieldset>
        <legend><?= __('Add Student Account') ?></legend>
        <?php
            echo $this->Form->control('month_id');
            echo $this->Form->control('student_id', ['options' => $students]);
            echo $this->Form->control('amount');
            echo $this->Form->control('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
