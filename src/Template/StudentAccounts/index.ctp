<div class="studentAccounts index large-9 medium-8 columns content">
    <h3><?= __('Student Accounts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('month_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('student_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($studentAccounts as $studentAccount): ?>
            <tr>
                <td><?= $this->Number->format($studentAccount->id) ?></td>
                <td><?= $studentAccount->has('month') ? $this->Html->link($studentAccount->month->name, ['controller' => 'Months', 'action' => 'view', $studentAccount->month->id]) : '' ?></td>
                <td><?= $studentAccount->has('student') ? $this->Html->link($studentAccount->student->name, ['controller' => 'Students', 'action' => 'view', $studentAccount->student->id]) : '' ?></td>
                <td><?= $this->Number->format($studentAccount->amount) ?></td>
                <td><?= h($studentAccount->date) ?></td>
                <td><?= h($studentAccount->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $studentAccount->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $studentAccount->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $studentAccount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $studentAccount->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
