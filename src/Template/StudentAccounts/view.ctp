<div class="studentAccounts view large-9 medium-8 columns content">
    <h3><?= h($studentAccount->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Student') ?></th>
            <td><?= $studentAccount->has('student') ? $this->Html->link($studentAccount->student->name, ['controller' => 'Students', 'action' => 'view', $studentAccount->student->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($studentAccount->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Month Id') ?></th>
            <td><?= $this->Number->format($studentAccount->month_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($studentAccount->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($studentAccount->date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($studentAccount->created) ?></td>
        </tr>
    </table>
</div>
