<div class="months form large-9 medium-8 columns content">
    <?= $this->Form->create($month) ?>
    <fieldset>
        <legend><?= __('Edit Month') ?></legend>
        <?php
            echo $this->Form->control('name');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
