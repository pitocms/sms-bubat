<div class="months view large-9 medium-8 columns content">
    <h3><?= h($month->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($month->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($month->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($month->created) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Student Accounts') ?></h4>
        <?php if (!empty($month->student_accounts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Month Id') ?></th>
                <th scope="col"><?= __('Student Id') ?></th>
                <th scope="col"><?= __('Amount') ?></th>
                <th scope="col"><?= __('Date') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($month->student_accounts as $studentAccounts): ?>
            <tr>
                <td><?= h($studentAccounts->id) ?></td>
                <td><?= h($studentAccounts->month_id) ?></td>
                <td><?= h($studentAccounts->student_id) ?></td>
                <td><?= h($studentAccounts->amount) ?></td>
                <td><?= h($studentAccounts->date) ?></td>
                <td><?= h($studentAccounts->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'StudentAccounts', 'action' => 'view', $studentAccounts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'StudentAccounts', 'action' => 'edit', $studentAccounts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'StudentAccounts', 'action' => 'delete', $studentAccounts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $studentAccounts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
