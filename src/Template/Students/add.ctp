<div class="students form large-9 medium-8 columns content">
    <?= $this->Form->create($student) ?>
    <fieldset>
        <legend><?= __('Student Admission') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('roll');
            echo $this->Form->control('department_id', ['options' => $departments]);
            echo $this->Form->control('semester_id', ['options' => $semesters]);
            echo $this->Form->control('admission_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
