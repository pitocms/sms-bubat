<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * StudentAccounts Controller
 *
 * @property \App\Model\Table\StudentAccountsTable $StudentAccounts
 *
 * @method \App\Model\Entity\StudentAccount[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StudentAccountsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Months', 'Students']
        ];
        $studentAccounts = $this->paginate($this->StudentAccounts);

        $this->set(compact('studentAccounts'));
    }

    /**
     * View method
     *
     * @param string|null $id Student Account id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $studentAccount = $this->StudentAccounts->get($id, [
            'contain' => ['Months', 'Students']
        ]);

        $this->set('studentAccount', $studentAccount);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $studentAccount = $this->StudentAccounts->newEntity();
        if ($this->request->is('post')) {
            $studentAccount = $this->StudentAccounts->patchEntity($studentAccount, $this->request->getData());
            if ($this->StudentAccounts->save($studentAccount)) {
                $this->Flash->success(__('The student account has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The student account could not be saved. Please, try again.'));
        }
        $months = $this->StudentAccounts->Months->find('list', ['limit' => 200]);
        $students = $this->StudentAccounts->Students->find('list', ['limit' => 200]);
        $this->set(compact('studentAccount', 'months', 'students'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Student Account id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $studentAccount = $this->StudentAccounts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $studentAccount = $this->StudentAccounts->patchEntity($studentAccount, $this->request->getData());
            if ($this->StudentAccounts->save($studentAccount)) {
                $this->Flash->success(__('The student account has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The student account could not be saved. Please, try again.'));
        }
        $months = $this->StudentAccounts->Months->find('list', ['limit' => 200]);
        $students = $this->StudentAccounts->Students->find('list', ['limit' => 200]);
        $this->set(compact('studentAccount', 'months', 'students'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Student Account id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $studentAccount = $this->StudentAccounts->get($id);
        if ($this->StudentAccounts->delete($studentAccount)) {
            $this->Flash->success(__('The student account has been deleted.'));
        } else {
            $this->Flash->error(__('The student account could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
