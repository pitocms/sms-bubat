<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StudentAccount Entity
 *
 * @property int $id
 * @property int $month_id
 * @property int $student_id
 * @property float $amount
 * @property \Cake\I18n\FrozenDate $date
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Month $month
 * @property \App\Model\Entity\Student $student
 */
class StudentAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'month_id' => true,
        'student_id' => true,
        'amount' => true,
        'date' => true,
        'created' => true,
        'month' => true,
        'student' => true
    ];
}
