<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Routine Entity
 *
 * @property int $id
 * @property int $department_id
 * @property int $semester_id
 * @property int $subject_id
 * @property int $teacher_id
 * @property int $room_id
 * @property int $day_id
 * @property \Cake\I18n\FrozenTime $starting_hour
 * @property \Cake\I18n\FrozenTime $ending_hour
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Semester $semester
 * @property \App\Model\Entity\Subject $subject
 * @property \App\Model\Entity\Teacher $teacher
 * @property \App\Model\Entity\Room $room
 * @property \App\Model\Entity\Day $day
 */
class Routine extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'department_id' => true,
        'semester_id' => true,
        'subject_id' => true,
        'teacher_id' => true,
        'room_id' => true,
        'day_id' => true,
        'starting_hour' => true,
        'ending_hour' => true,
        'created' => true,
        'modified' => true,
        'department' => true,
        'semester' => true,
        'subject' => true,
        'teacher' => true,
        'room' => true,
        'day' => true
    ];
}
