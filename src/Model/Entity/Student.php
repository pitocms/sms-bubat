<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Student Entity
 *
 * @property int $id
 * @property string $name
 * @property string $roll
 * @property int $department_id
 * @property int $semester_id
 * @property \Cake\I18n\FrozenDate $admission_date
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\Department $department
 * @property \App\Model\Entity\Semester $semester
 */
class Student extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'roll' => true,
        'department_id' => true,
        'semester_id' => true,
        'admission_date' => true,
        'created' => true,
        'department' => true,
        'semester' => true
    ];
}
